from datetime import datetime, date

from models.Chambre.chambre import Chambre
from models.Client.client import Client
from models.Contrat.contrat import Contrat
from models.Penalites.penalites import Penalites
from models.Receptionise.receptioniste import Receptionise


def mettreLibre(self):
    self.etat = "libre"
    print("chambre liberée")


def mettrePrise(self):
    self.etat = "occupee"
    print("chambre occupée")


def signalerEnMaintenance(self):
    self.etat = "maintenance"
    print("chambre en maintenace")


def etat(self):
    return self.etat


def ajouter_penalite(self):
    detail_motif = input("Entrez le motif de la penalité")
    montant = input("Entrez le montant de la penalité")
    date = input("Entrez la date de la penalité")
    self.penalite.append(Penalites(montant, detail_motif, date, self.client))
    print("penalité ajoutée")


def montant_total(self):
    somme = 0
    if self.penalite.lenght > 0:
        for i in self.penalite:
            somme = somme + i.montant
    return self.prix_de_base + somme


def modifier_duree(self, newDuree):
    self.duree = newDuree
    self.prix_de_base = self.chambre.prix_chambre * newDuree
    print("durée modifiée")


def enregistrerClient(self):
    nom = input("Entrez le nom du client")
    numCNI = input("Entrez le numero de CNI du client")
    telephone = input("Entrez le numero de téléphone du client")

    nouv_client = Client(nom, numCNI, telephone)
    print("enregistrement client")
    return nouv_client


def enregistrerChambre(self):
    nom_chambre = input("Entrez le nom de la chambre")
    prix_chambre = input("Entrez le prix de la chambre")
    nombre_piece = input("Nombre de pièces de la chambre")
    superficie = input("Quelle est la superficie de la chambre ?")
    details = input("Entrez les details sur la chambre")
    # enregistrement nouveau client
    nouv_chambre = Chambre(nom_chambre, nombre_piece, prix_chambre, superficie, details)
    print("enreistrement chambre")
    return nouv_chambre


def enregistrerContrat(chambre, listeClient):
    # duree, prix_de_base,chambre, client
    # on passe directement l'id de la chambre apres avoir verifier que la dite chambre existe
    # on passe ici la liste des client car la donnee est statique dans notre __Main__
    # global nouv_contrat
    global duree
    try:
        duree = int(input("Entrez la durée du contrat"))
    except ValueError:
        print("entrer un bon format de date \n")
    client = input("Entrez le code du client\n")
    e = 0
    for i in listeClient:
        if i.id == client:
            client = i
            e = 1
    if e == 0:
        client = enregistrerClient(self=any)
        titre = input("Entrez l'intitule du contrat\n")
        nouv_contrat = Contrat(duree, chambre, client.id_client, titre)  # pourquoi id_receptioniste ?

        return f"le contrat est {nouv_contrat}"


# def enregistrerPaiement(self, contrat):
#     motif = input("Entrez le motif du paiement")
#     montant = input("Entrez le montant du paiement")
#     date = input("Entrez la date du paiement")
#     nouv_paiement = Paiement(montant, date, motif)

# def controlerPaiement(self, contrat):
#     print("etat paiement")


def marquerExploite(self):
    self.etat = "réglé et exploité"
    return "ce payement est desormais inutilisable pour d'autres reservations\n"


def retrouverChambreDisponible(listechambres):
    dispo = []
    for i in listechambres:
        if i.etat == "libre":
            dispo.append(i)
    return dispo


def retrouverUneChambre(id, listechambres):
    for i in listechambres:
        if i.id_chambre == id:
            return i
    return any


def libererChambre(listechambres):
    id = int(input("entrer l'id de la chambre a liberer\n"))
    for i in listechambres:
        if i.id_chambre == id:
            i.mettreLibre()
            return "chambre libéré !!"
    return f"il n'existe pas de chambre avec cet {id}\n"


def signalerChambreEnMaintenance(listechambres):
    id = int(input(" Entrer l'id de la chambre a liberer \n"))
    for i in listechambres:
        if i.id_chambre == id:
            i.signalerEnMaintenance()
            return " chambre en maintenance !!"
    return " il n'existe pas de chambre avec cet id"


def accueil():
    print("*--------------------------------------------------------------------------------------------------*\n")
    print("************************************ BIENVENUE A THE BEST HOTEL ************************************\n")
    print("*--------------------------------------------------------------------------------------------------*\n")


    print(f"*----------bonjour il est {date.today()} commençons une nouvelle journée--------------------------*\n")
    # code_receptioniste = int(input("entrer votre code receptioniste"))

    liste_receptioniste = [
        Receptionise(1,"nana","5456556","54545656","454656556","hjdhjhehj",2021),
        Receptionise(2,"steve","565656","hjhrhjezr","kjdsfkj","sdfhshj",2022),
    ]
    code_receptioniste = int(input("entrer votre code receptioniste\n"))

    for i in range(0,len(liste_receptioniste)):
        if liste_receptioniste[i].code == code_receptioniste:
            menu()
        else:
            print("Enregistrez-vous pour commencer !\n")
            code = input("entrer le code receptioniste\n")
            nom = input("Entrez votre nom\n")
            numCNI = input("Entrez votre numero de CNI\n")
            telephone = input("Entrez votre numero de telephone\n")
            dateNaissance = input("Entrez votre date de naissance\n")
            adresse = input("Entrez votre adresse\n")
            annee_embauche = input("Entrez votre année d'embauche\n")
            nouv_receptioniste = Receptionise(code ,nom, numCNI, telephone, dateNaissance, adresse, annee_embauche)
            return nouv_receptioniste


def menu():
    print("*--------------------------------------------------------------------------------------------------*\n")
    print("************************************ BIENVENUE A THE BEST HOTEL ************************************\n")
    print("*--------------------------------------------------------------------------------------------------*\n")

    print("Voici quelques uns des services que nous proposons")

    print("1-Enregistrer une chambre\n")
    print("2-Enregistrer un client\n")
    print("3-Louer une chambre\n")
    print("4-Liberer une chambre\n")
    print("5-Signaler une chambre en maintenance\n")
    print("6-Liste des chambres disponibles\n")
    choise = int(input("En quoi pouvons nous vous être utile ?  \n"))
    print(f"choix {choise}")
    return choise

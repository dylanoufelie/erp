from models.Paiement.paiement import Paiement


class Penalites:
    id_penalite = 0
    montant = 0
    detail_motif = ''
    date = ''
    client = any

    def __init__(self, montant, detail_motif, date, client):  # constructeur
        self.id_penalite = self.id_penalite + 1
        self.montant = montant
        self.detail_motif = detail_motif
        self.date = date
        self.client = client
        print("Penalité créée")


class Paiement_penalites(Paiement):
    id_p_penalites = 0

    def __init__(self, montant, date, motif):
        self.id_p_penalites = self.id_p_penalites + 1
        super().__init__(montant, date, motif)
        print("paiement penalités réglé")
